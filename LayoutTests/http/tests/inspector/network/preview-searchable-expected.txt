CONSOLE MESSAGE: line 6: XHR loaded: 1
CONSOLE MESSAGE: line 6: XHR loaded: 2
CONSOLE MESSAGE: line 6: XHR loaded: 3
CONSOLE MESSAGE: line 6: XHR loaded: 4
CONSOLE MESSAGE: line 6: XHR loaded: 5
CONSOLE MESSAGE: line 6: XHR loaded: 6
CONSOLE MESSAGE: line 6: XHR loaded: 7
CONSOLE MESSAGE: line 6: XHR loaded: 8
Tests that resources with JSON MIME types are previewed with the JSON viewer.


Running: plainTextTest
Is Searchable: true
Type: ResourceSourceFrame
Should have found and highlighted all: foo
Normal search found 0 results in dom.
CodeMirror search found 2 results in dom.


Running: jsonTest
Is Searchable: true
Type: JSONView
Should have found and highlighted all: 533
Normal search found 1 results in dom.
CodeMirror search found 0 results in dom.

Should have found and highlighted all: 322
Normal search found 1 results in dom.
CodeMirror search found 0 results in dom.


Running: jsonSpecialMimeTest
Is Searchable: true
Type: JSONView
Should have found and highlighted all: foo
Normal search found 2 results in dom.
CodeMirror search found 0 results in dom.


Running: xmlMultipleSearchTest
Is Searchable: false
Type: XMLView

Running: xmlSingleSearchTest
Is Searchable: false
Type: XMLView

Running: xmlCommentSearchTest
Is Searchable: false
Type: XMLView

Running: xmlCDATASearchTest
Is Searchable: false
Type: XMLView

Running: xmlMimeTypeJsonTest
Is Searchable: true
Type: JSONView
Should have found and highlighted all: fooo
Normal search found 1 results in dom.
CodeMirror search found 0 results in dom.

Should have found and highlighted all: bar
Normal search found 2 results in dom.
CodeMirror search found 0 results in dom.


