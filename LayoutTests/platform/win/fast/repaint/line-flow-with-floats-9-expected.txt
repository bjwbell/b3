{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [0, 0, 500, 600],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 485, 600],
          "reason": "full"
        },
        {
          "object": "LayoutBlockFlow P",
          "rect": [8, 569, 418, 21],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow P",
          "rect": [8, 572, 418, 18],
          "reason": "incremental"
        },
        {
          "object": "LayoutText #text",
          "rect": [14, 494, 406, 90],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [14, 476, 406, 90],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [14, 440, 355, 53],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [14, 422, 355, 53],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [14, 404, 355, 36],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [14, 404, 354, 36],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [65, 386, 304, 18],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [65, 368, 304, 18],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [65, 386, 303, 36],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [178, 350, 146, 18],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [65, 368, 145, 18],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (floating) SPAN id='greenFloat'",
          "rect": [372, 389, 48, 81],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (floating) SPAN id='greenFloat'",
          "rect": [372, 371, 48, 81],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (floating) SPAN id='yellowFloat'",
          "rect": [372, 245, 48, 49],
          "reason": "layoutObject insertion"
        },
        {
          "object": "LayoutView #document",
          "rect": [485, 0, 15, 600],
          "reason": "incremental"
        },
        {
          "object": "LayoutView #document",
          "rect": [485, 0, 15, 600],
          "reason": "scroll"
        },
        {
          "object": "InlineTextBox ' twist itself round and look up in her face, with\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox ' was in a furious passion, and went\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Alice began to feel very uneasy: to be sure, she had not as\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Alice began to feel very uneasy: to be sure, she had not as\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'Queen'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'The chief difficulty Alice found at first was in'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'The players all played at once without waiting'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'The players all played at once without waiting'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'a very short time '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'a very short time '",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'about once in a minute.\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'about, and shouting \u2018Off with his head!\u2019 or \u2018Off with'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'arm, with its legs hanging down,\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'become of\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'become of\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'begin again, it was very provoking to find that the hedgehog'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'besides all\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'body tucked away,\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'but generally, just as'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'comfortably enough, under her'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'difficult game indeed.\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'difficult game indeed.\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'doubled-up\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'flamingo: she succeeded in getting its'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'for the hedgehogs; and in\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'for the hedgehogs; and in\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'for turns,\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'for turns,\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'going to give the hedgehog a blow with its head, it'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'had\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'had any dispute with the Queen, but she knew that it might'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'had any dispute with the Queen, but she knew that it might'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'happen any minute, \u2018and then,\u2019 thought she, \u2018what would'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'happen any minute, \u2018and then,\u2019 thought she, \u2018what would'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'her head!\u2019 about once in a minute.\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'her head!\u2019'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'here; the great\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'here; the great\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'here; the great\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'laughing: and when she had got its head down, and was going'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'managing her\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'me? They\u2019re dreadfully fond of beheading people'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'me? They\u2019re dreadfully fond of beheading people'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'other parts of\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'out, and was'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'puzzled expression that she could not help bursting out'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'quarrelling all the while, and fighting'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'quarrelling all the while, and fighting'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'she had got its neck nicely straightened\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'she wanted to send the hedgehog to, and, as the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'shouting \u2018Off with his head!\u2019 or \u2018Off with\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'soldiers were always getting up and walking off to'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'stamping about, and'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'stamping'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'such a'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'that it was a very\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the Queen'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'the ground, Alice soon came to the conclusion'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'this, there was generally a ridge or furrow in the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'to\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'unrolled itself, and was in the act of crawling away:'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'was in a furious passion, and went\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'way wherever\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'wonder is, that there\u2018s any one left alive!\u2019'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'wonder is, that there\u2018s any one left alive!\u2019'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'wonder is, that there\u2018s any one left alive!\u2019'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'would'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'yet'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'yet'",
          "reason": "bounds change"
        },
        {
          "object": "LayoutView #document",
          "reason": "scroll"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "VerticalScrollbar",
          "reason": "scroll"
        }
      ]
    }
  ]
}

