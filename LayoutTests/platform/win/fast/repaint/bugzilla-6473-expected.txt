{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='allcontent'",
          "rect": [8, 130, 784, 50],
          "reason": "style change"
        },
        {
          "object": "LayoutBlockFlow (relative positioned) P",
          "rect": [8, 146, 784, 18],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (relative positioned) P",
          "rect": [8, 130, 784, 18],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [8, 146, 70, 17],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [8, 130, 70, 17],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'SUCCESS'",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

