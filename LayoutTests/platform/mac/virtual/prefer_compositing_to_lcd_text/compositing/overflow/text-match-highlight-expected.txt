{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutText #text",
          "rect": [282, 36, 45, 18],
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'findme'",
          "reason": "full"
        }
      ],
      "children": [
        {
          "position": [0, 54],
          "bounds": [800, 500],
          "shouldFlattenTransform": false,
          "drawsContent": true,
          "paintInvalidations": [
            {
              "object": "LayoutSVGInlineText #text",
              "rect": [10, 72, 227, 18],
              "reason": "full"
            },
            {
              "object": "LayoutSVGInlineText #text",
              "rect": [20, 160, 200, 72],
              "reason": "full"
            },
            {
              "object": "LayoutSVGInlineText #text",
              "rect": [10, 126, 139, 12],
              "reason": "full"
            },
            {
              "object": "LayoutText #text",
              "rect": [268, 0, 46, 18],
              "reason": "full"
            },
            {
              "object": "LayoutText #text",
              "rect": [90, 0, 46, 18],
              "reason": "full"
            },
            {
              "object": "LayoutText #text",
              "rect": [224, 0, 45, 18],
              "reason": "full"
            },
            {
              "object": "LayoutText #text",
              "rect": [52, 18, 45, 18],
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'Can you findme in this boring text?'",
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'Findme in a typewriter!'",
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'Findme on a path! Did you findme?'",
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'findme'",
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'findme'",
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'findme'",
              "reason": "full"
            },
            {
              "object": "InlineTextBox 'findme'",
              "reason": "full"
            }
          ],
          "children": [
            {
              "bounds": [785, 485],
              "shouldFlattenTransform": false,
              "children": [
                {
                  "bounds": [785, 1340],
                  "drawsContent": true,
                  "paintInvalidations": [
                    {
                      "object": "LayoutSVGInlineText #text",
                      "rect": [10, 72, 227, 18],
                      "reason": "full"
                    },
                    {
                      "object": "LayoutSVGInlineText #text",
                      "rect": [20, 160, 200, 72],
                      "reason": "full"
                    },
                    {
                      "object": "LayoutSVGInlineText #text",
                      "rect": [10, 126, 139, 12],
                      "reason": "full"
                    },
                    {
                      "object": "LayoutText #text",
                      "rect": [268, 0, 46, 18],
                      "reason": "full"
                    },
                    {
                      "object": "LayoutText #text",
                      "rect": [90, 0, 46, 18],
                      "reason": "full"
                    },
                    {
                      "object": "LayoutText #text",
                      "rect": [224, 0, 45, 18],
                      "reason": "full"
                    },
                    {
                      "object": "LayoutText #text",
                      "rect": [52, 18, 45, 18],
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'Can you findme in this boring text?'",
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'Findme in a typewriter!'",
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'Findme on a path! Did you findme?'",
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'findme'",
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'findme'",
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'findme'",
                      "reason": "full"
                    },
                    {
                      "object": "InlineTextBox 'findme'",
                      "reason": "full"
                    }
                  ]
                }
              ]
            },
            {
              "bounds": [800, 500],
              "children": [
                {
                  "position": [0, 485],
                  "bounds": [785, 15]
                },
                {
                  "position": [785, 0],
                  "bounds": [15, 485]
                },
                {
                  "position": [785, 485],
                  "bounds": [15, 15],
                  "drawsContent": true
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}

