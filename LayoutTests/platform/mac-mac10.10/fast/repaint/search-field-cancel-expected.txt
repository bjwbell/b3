{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='inner-editor'",
          "rect": [17, 45, 108, 13],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [17, 45, 50, 13],
          "reason": "layoutObject insertion"
        },
        {
          "object": "InlineTextBox 'some text'",
          "reason": "layoutObject insertion"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

