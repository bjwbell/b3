{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow P",
          "rect": [8, 400, 418, 39],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutText #text",
          "rect": [297, 420, 72, 19],
          "reason": "style change"
        },
        {
          "object": "LayoutText #text",
          "rect": [303, 420, 67, 19],
          "reason": "style change"
        },
        {
          "object": "InlineTextBox ''",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'a very short time '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hedgehogs; and in\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'quarrelling all the while, and fighting for the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the Queen'",
          "reason": "style change"
        },
        {
          "object": "InlineTextBox 'turns,\n'",
          "reason": "full"
        },
        {
          "object": "LayoutInline SPAN id='theQueen'",
          "reason": "style change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

