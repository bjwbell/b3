{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow P",
          "rect": [8, 340, 418, 99],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow (floating) SPAN id='blueFloat'",
          "rect": [14, 363, 48, 65],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [28, 363, 20, 20],
          "reason": "layoutObject insertion"
        },
        {
          "object": "InlineTextBox 'The players all played at once without waiting\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'a very short time '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'difficult game indeed.\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'foo'",
          "reason": "layoutObject insertion"
        },
        {
          "object": "InlineTextBox 'for'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hedgehogs; and in\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'other parts of\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'quarrelling all the while, and fighting for the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the Queen'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the ground, Alice soon came to the conclusion that it'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'turns,\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'was a very\n'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

