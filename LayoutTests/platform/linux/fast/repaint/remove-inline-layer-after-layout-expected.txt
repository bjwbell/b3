{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [8, 108, 784, 104],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutInline SPAN id='target'",
          "rect": [112, 108, 171, 104],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutText #text",
          "rect": [112, 193, 171, 19],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [282, 108, 101, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [112, 108, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "full"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'This span should disappear.\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'This span should disappear.\n'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

