{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutInline A id='link'",
          "rect": [-1, -1, 802, 234],
          "reason": "style change"
        },
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [-1, -1, 801, 21],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow P",
          "rect": [0, 36, 800, 19],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "InlineFlowBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "style change"
        },
        {
          "object": "InlineTextBox 'Home'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'crbug.com/424078: ensure inline elements get their outline painted'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

