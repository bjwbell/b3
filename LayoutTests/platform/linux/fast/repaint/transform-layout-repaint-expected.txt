{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='target'",
          "rect": [51, 50, 197, 88],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutText #text",
          "rect": [52, 51, 42, 31],
          "reason": "full"
        },
        {
          "object": "InlineTextBox ''",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'PASS'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

