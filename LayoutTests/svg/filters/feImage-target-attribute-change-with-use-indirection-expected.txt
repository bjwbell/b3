{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGRect rect",
          "rect": [0, 0, 110, 110],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRect rect id='rect'",
          "rect": [0, 0, 100, 100],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRect rect id='rect'",
          "rect": [0, 0, 100, 100],
          "reason": "layoutObject insertion"
        },
        {
          "object": "LayoutSVGContainer use id='use'",
          "rect": [1, 0, 99, 100],
          "reason": "incremental"
        },
        {
          "object": "LayoutSVGRect rect id='rect'",
          "rect": [0, 0, 1, 100],
          "reason": "layoutObject removal"
        }
      ]
    }
  ]
}

