{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGContainer g",
          "rect": [122, 127, 66, 66],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGPath path id='path'",
          "rect": [122, 127, 66, 66],
          "reason": "style change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [122, 127, 66, 66],
          "reason": "bounds change"
        }
      ]
    }
  ]
}

