{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow div id='contentBox'",
          "rect": [8, 50, 402, 402],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutSVGContainer use",
          "rect": [27, 67, 364, 364],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGPath path",
          "rect": [27, 67, 364, 364],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [27, 67, 364, 364],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGViewportContainer svg id='gamesBorder'",
          "rect": [27, 67, 364, 364],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGContainer use",
          "rect": [13, 205, 92, 91],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGPath path",
          "rect": [13, 205, 92, 91],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [13, 205, 92, 91],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGViewportContainer svg id='gamesBorder'",
          "rect": [13, 205, 92, 91],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

