# These tests currently fail when they run with --site-per-process.
# See https://crbug.com/477150.

# https://crbug.com/611838 - regression when geolocation tests use JS mock.
http/tests/security/powerfulFeatureRestrictions/geolocation-on-secure-origin-in-insecure-origin.html [ Timeout ]

# https://crbug.com/611154 - DCHECK in NavigatorImpl::DidStartProvisionalLoad.
http/tests/security/upgrade-insecure-requests/iframe-upgrade.https.html [ Crash ]

# https://crbug.com/582494 - [sigsegv / av] blink::Document::styleEngine.
http/tests/security/mixedContent/insecure-plugin-in-iframe.html [ Crash ]

# https://crbug.com/582245 - no exception, b/c BindingSecurity::shouldAllowAccessTo exits early when |!target|.
http/tests/security/xss-DENIED-getSVGDocument-iframe.html [ Failure ]
http/tests/security/xss-DENIED-getSVGDocument-object.html [ Failure ]
http/tests/security/xssAuditor/block-does-not-leak-location.html [ Failure ]
http/tests/security/xssAuditor/block-does-not-leak-referrer.html [ Failure ]
http/tests/security/xssAuditor/full-block-script-tag-cross-domain.html [ Failure ]

# https://crbug.com/582211 - body of POST request is not delivered to XSSAuditor.
http/tests/security/xssAuditor/full-block-post-from-iframe.html [ Failure ]
http/tests/security/xssAuditor/xss-protection-parsing-01.html [ Failure ]
http/tests/security/xssAuditor/post-from-iframe.html [ Failure ]
http/tests/security/xssAuditor/script-tag-post.html [ Failure ]
http/tests/security/xssAuditor/xss-filter-bypass-long-string.html [ Failure ]
http/tests/security/xssAuditor/script-tag-post-control-char.html [ Failure ]
http/tests/security/xssAuditor/script-tag-post-null-char.html [ Failure ]

# https://crbug.com/582176 - InspectorTest.changeExecutionContext doesn't like OOPIFs.
http/tests/inspector/console-cd-completions.html [ Failure ]
http/tests/inspector/console-cd.html [ Failure ]

# https://crbug.com/608015 - node.contentDocument is undefined.
http/tests/inspector-protocol/access-inspected-object.html [ Failure Timeout ]

# https://crbug.com/608023 - does injecting injections work with OOPIFs?
http/tests/inspector/injected-script-for-origin.html [ Failure ]

# https://crbug.com/554119 - Popup menu is in the wrong location.
http/tests/webfont/popup-menu-load-webfont-after-open.html [ Failure ]

# https://crbug.com/602493 - Layout tests harness doesn't support history dump across OOPIFs
http/tests/security/mixedContent/insecure-iframe-in-main-frame.html [ Crash ]

# https://crbug.com/582522 - extra mixedContent checks reported with --site-per-process
# https://crbug.com/602497 - Inconsistent console messages about mixed content,
#                            when running with or without --site-per-process
http/tests/security/mixedContent/active-subresource-in-http-iframe-not-blocked.https.html [ Failure ]
http/tests/security/mixedContent/insecure-iframe-in-iframe.html [ Failure ]
http/tests/security/mixedContent/insecure-iframe-in-main-frame-allowed.html [ Failure ]

# https://crbug.com/585171 - iframe restored from history should be excluded from performance entries.
http/tests/misc/resource-timing-iframe-restored-from-history.html [ Failure Timeout ]

# https://crbug.com/585194 - back/forward lists look different with --site-per-process
http/tests/navigation/back-to-dynamic-iframe.html [ Failure Timeout ]
http/tests/navigation/back-to-redirect-with-frame.php [ Failure ]

# https://crbug.com/585188 - testRunner.addOriginAccessWhitelistEntry is not replicated to OOPIFs.
http/tests/xmlhttprequest/origin-whitelisting-all.html [ Failure ]
http/tests/xmlhttprequest/origin-whitelisting-ip-addresses.html [ Failure ]

# https://crbug.com/601584 - No OOPIF support for UserGestureIndicator triggers
#                            cross-origin-iframe.html layout test failure
http/tests/bluetooth/https/requestDevice/cross-origin-iframe.html [ Failure ]

# https://crbug.com/606594 - UaF of delegate_ in WebFrameTestClient::willSendRequest
http/tests/local/serviceworker/fetch-request-body-file.html [ Failure Crash ]

# https://crbug.com/607991 - MockWebClipboardImpl not replicated across OOPIFs.
http/tests/misc/copy-resolves-urls.html [ Failure ]

# https://crbug.com/607981 - trouble with application/x-blink-deprecated-test-plugin
http/tests/plugins/cross-frame-object-access.html [ Failure ]

# https://crbug.com/608780 - window.performance doesn't work with OOPIFs
http/tests/w3c/webperf/submission/Intel/resource-timing/test_resource_timing_cross_origin_redirect.html [ Timeout ]
http/tests/w3c/webperf/submission/Intel/resource-timing/test_resource_timing_cross_origin_redirect_with_timing_allow_origin.html [ Timeout ]
http/tests/w3c/webperf/submission/Intel/resource-timing/test_resource_timing_cross_origin_resource_request.html [ Timeout ]
http/tests/w3c/webperf/submission/Intel/resource-timing/test_resource_timing_timing_allow_cross_origin_resource_request.html [ Timeout ]

# https://crbug.com/585501 - csp directives are not replicated to OOPIFs
http/tests/security/contentSecurityPolicy/frame-src-child-frame-navigates-to-blocked-origin.html [ Failure ]

# TODO(alexmos,lukasza): Triage these failures and assign more specific bugs.
# Uninvestigated failures under http/tests/security:
http/tests/security/cross-origin-shared-worker-allowed.html [ Failure Timeout ]
http/tests/security/cross-origin-worker-indexeddb-allowed.html [ Failure Timeout ]
http/tests/security/frameNavigation/not-opener.html [ Timeout ]
http/tests/security/frameNavigation/xss-DENIED-targeted-link-navigation.html [ Timeout ]
http/tests/security/referrer-policy-redirect-link.html [ Timeout ]
# Uninvestigated failures under http/tests (but outside of http/tests/security):
http/tests/history/cross-origin-redirect-on-back.html [ Timeout ]
http/tests/inspector-protocol/request-mixed-content-status-blockable.html [ Timeout ]
http/tests/inspector-protocol/request-mixed-content-status-none.html [ Timeout ]
http/tests/inspector-protocol/request-mixed-content-status-optionally-blockable.html [ Timeout ]
http/tests/inspector/change-iframe-src.html [ Timeout ]
http/tests/inspector/console-cross-origin-iframe-logging.html [ Timeout ]
http/tests/inspector/extensions-headers.html [ Timeout ]
http/tests/inspector/extensions-iframe-eval.html [ Timeout ]
http/tests/inspector/extensions-ignore-cache.html [ Timeout ]
http/tests/inspector/extensions-network-redirect.html [ Timeout ]
http/tests/inspector/extensions-useragent.html [ Timeout ]
http/tests/inspector/indexeddb/resources-panel.html [ Timeout ]
http/tests/inspector/inspect-iframe-from-different-domain.html [ Timeout ]
http/tests/intersection-observer/iframe-cross-origin.html [ Timeout ]
http/tests/local/drag-over-remote-content.html [ Crash Timeout ]
http/tests/navigation/cross-origin-fragment-navigation-is-async.html [ Failure ]
http/tests/serviceworker/http-to-https-redirect-and-register.html [ Timeout ]
