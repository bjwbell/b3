{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (relative positioned) DIV id='block'",
          "rect": [208, 200, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (relative positioned) DIV id='block'",
          "rect": [8, 200, 100, 100],
          "reason": "bounds change"
        }
      ]
    }
  ]
}

