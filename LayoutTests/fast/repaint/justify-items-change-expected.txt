{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutGrid DIV id='container'",
          "rect": [0, 52, 200, 300],
          "reason": "layoutObject insertion"
        },
        {
          "object": "LayoutGrid DIV id='container'",
          "rect": [0, 52, 200, 300],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutBlockFlow DIV class='item'",
          "rect": [150, 52, 50, 300],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutBlockFlow DIV class='item'",
          "rect": [0, 52, 50, 300],
          "reason": "layoutObject insertion"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [150, 52, 50, 50],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [0, 52, 50, 50],
          "reason": "layoutObject insertion"
        }
      ]
    }
  ]
}

